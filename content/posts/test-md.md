---
title: "Test md"
---

## Section 1

```javascript
function test(a, b, c) {
	return `a=${a} b=${b} c=${c}`
}

test("av", "bv", "cv")
```

### Section 1.1

```bash
test() {
	local a=${1} b=${2} c=${3}
	shift 3
	return "a=${a} b=${b} c=${c}"
}

test "av" "bv" "cv"
```

#### Section 1.1.1

```python
def test(a, b, c):
	return "a={} b={} c={}".format(a, b, c)

test("av", "bv", "cv")
```

## Section 2

 - a
 - b
 - c
