
hugo_cmd=extra/bin/hugo-env hugo
hugo_flags=--noTimes --verbose
hugo=$(hugo_cmd) $(hugo_flags)

nanoc_cmd=nanoc
nanoc_flags=
nanoc=$(nanoc_cmd) $(nanoc_flags)


%/:
	mkdir -p $(@)

publish: | public/
	$(hugo) --buildDrafts --cleanDestinationDir --enableGitInfo
	unzip -o -d public/ favicon_io.zip
	cp ./public/favicon-32x32.png ./public/favicon.png
	#$(nanoc) compile

serve:
	$(hugo) --buildDrafts --cleanDestinationDir --enableGitInfo server
	#$(nanoc) view --live-reload

clean:
	rm -vrf public/
	rm -vrf tmp/
	rm -vrf output/


gitlab-pages: publish
